
#. created on
#. 27/04/2020
#. Chih-Wei Tsai

sample_size_using_bland_altman <- function(mu, SD, delta, gamma=0.05, alpha=0.05, beta=0.20) {
  #. sample size estimation using Bland-Altman analysis -----
  #. This function built based on Lu et al. 2016_The International Journal of Biostatistics
  
  #. arguments in function
  #. 'mu'    <- expected mean of differences
  #. 'SD'    <- expected standard deviation of differences
  #. 'delta' <- expected maximum allowed differencesS
  #. 'gamma' <- 0.05
  #. 'alpha' <- 0.05 (type I error)
  #. 'beta'  <- 0.20 (type II error)
  
  upperLoA = mu + SD * stats::qnorm(1 - gamma/2, lower.tail=TRUE)
  lowerLoA = mu - SD * stats::qnorm(1 - gamma/2, lower.tail=TRUE)
  
  #. check whether delta is less than upper or lower LoAs -----
  if (delta > abs(upperLoA) & delta > abs(lowerLoA)) {

    for (i in c(1, 2, 3)) {
    
      # initial estimate n +++++
      zgamma <- stats::qnorm(1 - gamma/2, lower.tail=TRUE)
      numerator <- (2 + zgamma^2) * SD^2
      denominator <- 2 * (zgamma * SD - delta)^2
      fraction <- numerator/denominator
      zalpha <- stats::qnorm(1 - alpha/2, lower.tail=TRUE)
      prob <- 1 - beta/2
      tinv0 <- stats::qt(prob,  df = Inf, ncp = zalpha, lower.tail=TRUE)
      n0 <- fraction * tinv0^2
      
      # compute quantiles of t-distribution -----
      talpha <- stats::qt(1 - alpha/2, df = n0 - 1, lower.tail=TRUE)
      
      # compute standard error on LOA -----
      se <- SD * sqrt( (1/n0) + (zgamma^2)/(2*(n0 - 1)) )
      tau1 <- (delta - mu - zgamma * SD)/se # non-centrality parameter
      tau2 <- (delta + mu - zgamma * SD)/se # non-centrality parameter
      beta1 <- 1 - stats::pt(talpha, df = n0 - 1, ncp = tau1, lower.tail=FALSE) # Lu eq 3
      beta2 <- 1 - stats::pt(talpha, df = n0 - 1, ncp = tau2, lower.tail=FALSE) # Lu eq 4
      power <- 1 - (beta1 + beta2) # Lu eq 5
      
      n <- n0
      nPrev <- 0
      #. iterate n until the convergence has occurred -----
      while (abs(nPrev - n) > 0.0000000001) {
        
        nPrev <- n
        talpha <- stats::qt(1 - alpha/2, df = n - 1, lower.tail=TRUE)
        tinv   <- stats::qt(prob,  df = n - 1, ncp = talpha, lower.tail=TRUE)
        n <- fraction * tinv^2 # Lu eq 6
        
        }
      
      n <- round(n + 1, 0)
      #. estimate power when the convergence of n has occurred -----
      while (power < (1-beta)) {
        
        n <- n + 1
        talpha <- stats::qt(1 - alpha/2, df = n - 1, lower.tail=TRUE)
        se <- SD * sqrt( (1/n) + (zgamma^2)/(2*(n - 1)) )
        tau1 <- (delta - mu - zgamma * SD)/se # non-centrality parameter
        tau2 <- (delta + mu - zgamma * SD)/se # non-centrality parameter
        beta1 <- 1 - stats::pt(talpha, df = n - 1, ncp = tau1, lower.tail=FALSE) # Lu eq 3
        beta2 <- 1 - stats::pt(talpha, df = n - 1, ncp = tau2, lower.tail=FALSE) # Lu eq 4
        power <- 1 - (beta1 + beta2) # Lu eq 5
        
        }
    }
    
    n <- round(n + 1, 0)
    return (n) 
  
  } else {
    print('Please provide a delta greater than estimated upper and lower LoAs!')
  }
  
}

#. example -----
n <- list()
mu <- 0.001167
SD <- 0.001129
delta <- seq(0.0034, SD*4, 0.0001)

for (i in delta) {
  process_name = paste('processing delta = ', i, sep='')
  print(process_name, quote=FALSE)
  n_ <- sample_size_using_bland_altman(mu=mu, SD=SD, delta=i)
  n <- append(n, n_)
  ssn = paste('n = ', n_, sep='')
  print(ssn, quote=FALSE)
  print('done', quote=FALSE)
  print('', quote=FALSE )
  }

ss_df <- data.frame('estimated_sample_size'=matrix(unlist(n), nrow=length(n), byrow=T))
ss_df$delta <- delta
ss_df$ratio_delta_SD <- ss_df$delta / SD
